<?php
/**
 * @file
 * re_videos.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function re_videos_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'videos';
  $context->description = 'Videos';
  $context->tag = 'Videos';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-videos-block' => array(
          'module' => 'views',
          'delta' => 'videos-block',
          'region' => 'content',
          'weight' => '-14',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Videos');
  $export['videos'] = $context;

  return $export;
}
