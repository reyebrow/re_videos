<?php
/**
 * @file
 * re_videos.features.content.inc
 */

/**
 * Implements hook_content_defaults().
 */
function re_videos_content_defaults() {
  $content = array();

  $content['Video_1'] = (object) array(
    'exported_path' => 'videos/kierian-lal-drupal-government',
    'title' => 'Kierian Lal on Drupal in Government',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'video',
    'language' => 'und',
    'created' => '1330124939',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'Video_1',
    'body' => array(),
    'field_video_link' => array(
      'und' => array(
        0 => array(
          'url' => 'http://www.youtube.com/watch?v=Fgfb62Yoc8w',
          'title' => NULL,
          'attributes' => array(),
        ),
      ),
    ),
  );

  $content['Video_2'] = (object) array(
    'exported_path' => 'videos/federal-government-experience',
    'title' => 'A Federal Government Experience ',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'video',
    'language' => 'und',
    'created' => '1330124966',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'Video_2',
    'body' => array(),
    'field_video_link' => array(
      'und' => array(
        0 => array(
          'url' => 'http://www.youtube.com/watch?v=zvOgEc3SDzQ',
          'title' => NULL,
          'attributes' => array(),
        ),
      ),
    ),
  );

  $content['Video_3'] = (object) array(
    'exported_path' => 'videos/high-profile-websites-powered-drupal',
    'title' => 'High Profile Websites Powered by Drupal',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'video',
    'language' => 'und',
    'created' => '1330124988',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'Video_3',
    'body' => array(),
    'field_video_link' => array(
      'und' => array(
        0 => array(
          'url' => 'http://www.youtube.com/watch?v=LOv94z7fk6w',
          'title' => NULL,
          'attributes' => array(),
        ),
      ),
    ),
  );

  $content['Video_4'] = (object) array(
    'exported_path' => 'videos/government-open-source-and-tech',
    'title' => 'Government, Open Source and Tech',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'video',
    'language' => 'und',
    'created' => '1330125049',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'Video_4',
    'body' => array(),
    'field_video_link' => array(
      'und' => array(
        0 => array(
          'url' => 'http://www.youtube.com/watch?v=NnazJZBypA8',
          'title' => NULL,
          'attributes' => array(),
        ),
      ),
    ),
  );

  $content['Video_5'] = (object) array(
    'exported_path' => 'videos/open-source-software-instrument-public-policy',
    'title' => 'Open Source Software as an Instrument of Public Policy ',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'video',
    'language' => 'und',
    'created' => '1330125063',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'Video_5',
    'body' => array(),
    'field_video_link' => array(
      'und' => array(
        0 => array(
          'url' => 'http://www.youtube.com/watch?v=8r-h7UWtsfs',
          'title' => NULL,
          'attributes' => array(),
        ),
      ),
    ),
  );

  $content['Video_6'] = (object) array(
    'exported_path' => 'videos/should-government-mandate-open-source-use',
    'title' => 'Should Government Mandate Open Source Use?',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'video',
    'language' => 'und',
    'created' => '1330125073',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'Video_6',
    'body' => array(),
    'field_video_link' => array(
      'und' => array(
        0 => array(
          'url' => 'http://www.youtube.com/watch?v=SfygezmQq9U',
          'title' => NULL,
          'attributes' => array(),
        ),
      ),
    ),
  );

return $content;
}
